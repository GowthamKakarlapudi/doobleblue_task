import { Component, OnInit } from '@angular/core';
import {AppointmentsAndBookingsService} from '../appointments-and-bookings.service'
import * as moment from 'moment';
import {Router} from '@angular/router'
@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {
  selected_date = moment(new Date()).format('YYYY-MM-DD');
  staff_details:any;
  staff_availability:any;
  slots:any;
  staff_obj:any;
  patient_name = '';
  patient_age = 0;
  patient_temprature = 98;
  patient_mobile = '';
  mobile_error = false;
  age_error = false;
  temprature_error = false;
  name_error = false;
  selected_slot = '';
  selected_staff_id = '';
  selected_staff_name = '';
  bookings:any;
  constructor(private router:Router,private appointmentAndBookings: AppointmentsAndBookingsService) { }

  async ngOnInit(){
    var step1 = 'Gowtham';
    var step2 = 'gowtham';
    var booking_details = [];
    await this.appointmentAndBookings.getStaffList().subscribe((data:any) => {this.staff_details = data.result;
    });
    await this.get_staff_slots();
  }
  async get_staff_slots(){
    var body = {hospital_id:1,selected_date:this.selected_date};
    this.appointmentAndBookings.getDoctorAvailabilityDetails(body).subscribe((data:any)=>{
      var result_data = data.result.doctor_details;
      var staff_data=[]
      for(var index = 0;index < result_data.length;index++){
        var slots=[];
        var start_time = result_data[index].start_time;
        let end_time = result_data[index].end_time;
        let start_time_array = result_data[index].start_time.split(":");
        let end_time_array = result_data[index].end_time.split(":");
        let continue_loop = true;
        while (continue_loop) {
          if(!slots.length){
            let minute = parseInt(start_time_array[1])+30;
            let hour :any;
            if(minute > 60) {
              hour = parseInt(start_time_array[0])+1;
              minute = minute-60;
            }else if(minute == 60){
              hour = parseInt(start_time_array[0])+1;
              minute = 0;
            }else {
              hour = parseInt(start_time_array[0]);
            }
            if(hour < 10){
              hour = '0'+hour;
            }
            else if(hour == 24){
              hour = '00';
            }
            var slot_time = hour+':'+minute;
            if(moment(slot_time,'HH:mm') <= moment(end_time,'HH:mm')){
              slots.push(start_time);
            }else {
              continue_loop = false;
            }

          }else{
            var split_slot_time = slots[slots.length-1].split(":");
            let minute:any = parseInt(split_slot_time[1])+30;
            let hour :any;
            if(minute > 60) {
              hour = parseInt(split_slot_time[0])+1;
              minute = minute-60;
            }else if(minute == 60){
              hour = parseInt(split_slot_time[0])+1;
              minute = '00';
            }else {
              hour = parseInt(split_slot_time[0]);
            }
            if(hour < 10){
              hour = '0'+hour;
            }
            else if(hour == 24){
              hour = '00';
            }
            var slot_time = hour+':'+minute;
            if(moment(slot_time,'HH:mm') <= moment(end_time,'HH:mm')){
              slots.push(slot_time);
            }else continue_loop = false;
          }
        }
        var tempObj={
          staff_id:result_data[index].doctor_id,
          staff_name:result_data[index].doctor_name,
          slots:slots
        }
        staff_data.push(tempObj);
        console.log(result_data[index]);
      }
      console.log(staff_data)
      this.staff_obj = staff_data;
      this.get_bookings();
    })
  }
  slotBooking(slot_time:any,staff_name:any){

    this.selected_slot = slot_time;
    this.selected_staff_name = staff_name;
    for(var index = 0;index < this.staff_details.length;index++){
      if(staff_name == this.staff_details[index].name){
        this.selected_staff_id = this.staff_details[index]._id;
      }
    }
  }
  confirmBooking(){
    this.validate();
    if(!this.age_error && !this.name_error && !this.mobile_error && !this.temprature_error){
      var booking_obj = {
        patient_name:this.patient_name,
        patient_age:this.patient_age,
        patient_temperature:this.patient_temprature,
        patient_mobile:this.patient_mobile,
        doctor_name:this.selected_staff_name,
        doctor_id:this.selected_staff_id,
        selected_slot:this.selected_slot,
        hospital_id:1
      }
      this.appointmentAndBookings.makeSlotBooking(booking_obj).subscribe((data:any) => {console.log(data);
      document.getElementById('closeModel')?.click();
        this.router.navigate(['/appointments']);
      });
    }
  }
  validate(){
    if(this.patient_mobile.length > 10 || this.patient_mobile.length < 10){
      this.mobile_error = true;
    } else  this.mobile_error = false;
    if(this.patient_name.length > 40 || this.patient_name.length < 3){
      this.name_error = true;
    } else this.name_error = false
    if(this.patient_temprature < 89 || this.patient_temprature > 115){
      this.temprature_error = true;
    }else this.temprature_error = false;
    if(this.patient_age < 1 || this.patient_age > 110){
      this.age_error = true;
    }else this.age_error = false;
  }
  get_bookings(){
      var getDoctorBookingsBody = {hospital_id:1}
      this.appointmentAndBookings.getDoctorBookings(getDoctorBookingsBody).subscribe((response : any)=>{
        this.bookings = response.result;
        for(var booking_index = 0;booking_index < this.bookings.length;booking_index++){
          for(var staff_index = 0;staff_index < this.staff_obj.length;staff_index++){
            console.log(this.bookings[booking_index].doctor_id);
            console.log(this.staff_obj[staff_index].staff_id);
            console.log(staff_index);
            console.log(this.staff_obj);
            if(this.bookings[booking_index].doctor_id == this.staff_obj[staff_index].staff_id){
              var index_of_slot = this.staff_obj[staff_index].slots.indexOf(this.bookings[booking_index].selected_slot);
              this.staff_obj[staff_index].slots.splice(index_of_slot,1);
              console.log(index_of_slot);
            }
          }
        }
      })
  }

}
