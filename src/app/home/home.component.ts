import { Time } from '@angular/common';
import { Component} from '@angular/core';
import * as moment from 'moment';
import {Router} from '@angular/router'
import {AppointmentsAndBookingsService} from '../appointments-and-bookings.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  show_add_staff = false;
  doctor_name = '';
  selected_doctor_name = '';
  selected_date = moment(new Date()).format('YYYY-MM-DD');
  doctors_list:any;
  selected_doctor_id:String = '';
  select_doctor_error = false;
  start_time:String = '08:00';
  end_time:String = '18:00';
  constructor(private router:Router,private appointmentAndBookings: AppointmentsAndBookingsService) { }

  staff_details :any;
  ngOnInit(): void {
    this.getStaffList();
  }
  getStaffList(){
    var body = {hospital_id:1}
    this.appointmentAndBookings.getStaffList().subscribe((data:any) => {this.staff_details = data.result;
      if(!this.staff_details.length)this.show_add_staff = true;
      else {
        this.show_add_staff = false;
        this.selected_doctor_name = this.staff_details[0].name;
        this.selected_doctor_id = this.staff_details[0]._id;
      }
    });

  }
  selectedDoctorName(){
    for(var index = 0;index < this.staff_details.length;index++){
      if(this.selected_doctor_id == this.staff_details[index].id){
        this.selected_doctor_name = this.staff_details[index].name
      alert(this.selected_doctor_name);
      }
    }
  }
  addSlot(){
    var doctor_availability_details = {
      doctor_id:this.selected_doctor_id,
      doctor_name:this.selected_doctor_name,
      hospital_id:1,
      selected_date:this.selected_date,
      start_time:this.start_time,
      end_time:this.end_time
    }
    this.appointmentAndBookings.addDoctorAvailability(doctor_availability_details).subscribe((data:any) =>{
      console.log(data.result)
      this.router.navigate(['/bookings']);
    })

  }
  async addStaff(){
    var details={hospital_id:1,name:this.doctor_name}
    await this.appointmentAndBookings.addStaff(details).subscribe(data =>{console.log(data)})
    this.show_add_staff = false;
    this.getStaffList();
  }

}
