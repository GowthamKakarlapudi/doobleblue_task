import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentsComponent } from './appointments/appointments.component';
import { HomeComponent } from './home/home.component';
import { BookingsComponent } from './bookings/bookings.component';

const routes: Routes = [
  { path: 'appointments', component: AppointmentsComponent},
  { path: 'home', component: HomeComponent},
  { path: 'bookings', component: BookingsComponent},
  { path: '**', component: HomeComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
