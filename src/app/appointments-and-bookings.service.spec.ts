import { TestBed } from '@angular/core/testing';

import { AppointmentsAndBookingsService } from './appointments-and-bookings.service';

describe('AppointmentsAndBookingsService', () => {
  let service: AppointmentsAndBookingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppointmentsAndBookingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
