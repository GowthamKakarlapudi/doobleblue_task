import { Component, OnInit } from '@angular/core';
import {AppointmentsAndBookingsService} from '../appointments-and-bookings.service'
@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {
  bookings:any;
  constructor(private appointmentAndBookings:AppointmentsAndBookingsService) {
   }

  ngOnInit(): void {
    var getDoctorBookingsBody = {hospital_id:1}
    this.appointmentAndBookings.getDoctorBookings(getDoctorBookingsBody).subscribe((response : any)=>{
      this.bookings = response.result;
    })
  }

}
