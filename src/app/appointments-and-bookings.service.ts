import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppointmentsAndBookingsService {

  constructor(private http:HttpClient) { }

  getStaffList(){
    return this.http.get('http://localhost:3000/get_list');
  }
  addStaff(data:any){
    return this.http.post('http://localhost:3000/add_Doctor',data);
  }
  addDoctorAvailability(data:any){
    return this.http.post('http://localhost:3000/add_doctor_availability',data);
  }
  getDoctorAvailabilityDetails(data:any){
    return this.http.post('http://localhost:3000/get_doctor_availability',data)
  }
  makeSlotBooking(data:any){
    return this.http.post('http://localhost:3000/make_slot_booking',data);
  }
  getDoctorBookings(data:any){
    return this.http.post('http://localhost:3000/get_bookings_by_hospital',data);
  }

}
